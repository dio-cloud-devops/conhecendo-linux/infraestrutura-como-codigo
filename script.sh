!#/bin/bash

echo "Criando diretorios"

mkdir /publico
mkdir /admin
mkdir /ven
mkdir /sec

echo "Criando grupos de usuarios"

groupadd GRP_ADMIN
groupadd GRP_VEN
groupadd GRP_SEC

echo "Criando usuarios"

useradd carlos -m -s /bin/bash -p $(openssl passwd -crypt Senha123) -G GRP_ADMIN
useradd maria -m -s /bin/bash -p $(openssl passwd -crypt Senha123) -G GRP_ADMIN
useradd joao -m -s /bin/bash -p $(openssl passwd -crypt Senha123) -G GRP_ADMIN

useradd debora -m -s /bin/bash -p $(openssl passwd -crypt Senha123) -G GRP_VEN
useradd sebastiao -m -s /bin/bash -p $(openssl passwd -crypt Senha123) -G GRP_VEN
useradd roberto -m -s /bin/bash -p $(openssl passwd -crypt Senha123) -G GRP_VEN

useradd josefina -m -s /bin/bash -p $(openssl passwd -crypt Senha123) -G GRP_SEC
useradd amanda -m -s /bin/bash -p $(openssl passwd -crypt Senha123) -G GRP_SEC
useradd rogerio -m -s /bin/bash -p $(openssl passwd -crypt Senha123) -G GRP_SEC

echo "Especificando permissoes dos diretorios"

chown root:GRP_ADMIN /admin
chown root:GRP_VEN /ven
chown root:GRP_SEC /sec

chmod 770 /admin
chmod 770 /ven
chmod 770 /sec
chmod 777 /publico

echo "Finalizado com sucesso."
